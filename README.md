## Introduction
Currently we do not offer an artifactory solution. Changes to this project, 
must be compiled, and the compiled version must be pushed to the precompile
project at https://gitlab.com/monosoft/itutil/precompile
