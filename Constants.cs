﻿using System.Globalization;

namespace ITUtil.Common.Constants
{
    public static class Culture
    {
        public static readonly CultureInfo DefaultCulture = new CultureInfo("en-US");
    }

    public static class RabbitMq
    {
        public const string EventExchangeName = "ms.events";
        public const string RequestExchangeName = "ms.request";
        public const string QueuePrefix = "ms.queue.";
    }
}
