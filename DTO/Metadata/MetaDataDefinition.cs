﻿// <copyright file="MetaDataDefinition.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    /// <summary>
    /// Metadata definition description
    /// </summary>
    public class MetaDataDefinition
    {
        public MetaDataDefinition(string servicename, string claimkey, DataContextEnum datacontext, params LocalizedString[] description)
        {
            this.key = $"{servicename}.{claimkey}";
            this.dataContext = datacontext;
            this.description = description;
        }

        public enum DataContextEnum
        {
            /// <summary>
            /// Claims that the user have given to the organisation, such as "can edit my profile", "allowed to send me a newsletter" etc.
            /// </summary>
            userClaims,
            /// <summary>
            /// Claims that the organisation have given to a user, suchs as "is database administrator"
            /// </summary>
            organisationClaims,
        }
        
        /// <summary>
        /// Gets or sets unique key for the scope
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets short DEVELOPER orientated description
        /// </summary>
        public LocalizedString[] description { get; set; }

        /// <summary>
        /// Gets or sets datacontext defines where this metadata is used (i.e. which metadatalist it should be saved to)
        /// </summary>
        public DataContextEnum dataContext { get; set; }
    }
}