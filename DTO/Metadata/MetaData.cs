﻿// <copyright file="MetaData.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    /// <summary>
    /// MetaData description
    /// </summary>
    public class MetaData
    {
        /// <summary>
        /// Gets or sets Key
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// Gets or sets Scope
        /// </summary>
        public string scope { get; set; }
    }
}