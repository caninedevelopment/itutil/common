﻿namespace ITUtil.Common.Utils.Bootstrapper
{
    public class RedisSettings
    {
        public string IP { get; set; }
        public int Port { get; set; }
        public override string ToString()
        {
            return $"{this.IP}:{this.Port}";
        }
    }

}
