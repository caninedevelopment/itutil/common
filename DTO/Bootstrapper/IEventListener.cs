﻿namespace ITUtil.Common.Utils.Bootstrapper
{
    public interface IEventListener
    {
        /// <summary>
        /// Should contain a unique service name
        /// </summary>
        string ServiceName { get; }

        /// <summary>
        /// List of operations that can be performed by this service
        /// </summary>
        MicroserviceDelegates.EventHandler Handler { get; }
    }

}
