﻿namespace ITUtil.Common.Utils.Bootstrapper
{
    public class GlobalRessources
    {
        public SQLSettings SQL { get; set; }
        public RedisSettings Redis { get; set; }
        public StorageSettings Storage { get; set; }
        public RabbitMQSettings RabbitMq { get; set; }
    }

}
