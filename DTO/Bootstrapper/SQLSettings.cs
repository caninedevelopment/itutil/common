﻿namespace ITUtil.Common.Utils.Bootstrapper
{
    using System;
    using System.Data;

    public class SQLSettings
    {
        public string Server { get; set; }
        public string DatabasePrefix { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public SQLType ServerType { get; set; }

        public string ConnectionString()
        {
            switch (this.ServerType)
            {
                case SQLType.MSSQL:
                    return this.ConnectionString("master");
                case SQLType.POSTGRESQL:
                    return this.ConnectionString("postgres");
                case SQLType.MYSQL:
                    return this.ConnectionString("mysql");
                default:
                    return this.ConnectionString("");
            }

        }

        private string GetFullDBName(string databasename)
        {
            if (databasename != "master" && databasename != "postgres" && databasename != "mysql" && databasename != "")
            {
                var prefix = DatabasePrefix == null ? "itutil" : DatabasePrefix;
                if (prefix.EndsWith("_")== false)
                {
                    prefix = prefix + "_";
                }

                return (prefix + databasename).ToLower();
            } else
            {
                return databasename.ToLower();
            }
        }

        public string ConnectionString(string databasename)
        {
            switch (this.ServerType)
            {
                case SQLType.MSSQL:
                    return $"Data Source={this.Server};Initial Catalog={GetFullDBName(databasename)};User id={this.Username};Password={this.Password}";
                case SQLType.POSTGRESQL:
                    return $"User ID={this.Username};Password={this.Password};Host={this.Server};Port=5432;Database={GetFullDBName(databasename)};";
                case SQLType.MYSQL:
                    return "";
                default:
                    return "";
            }
        }

        public void CreateDatabaseIfNoExists(System.Data.IDbConnection db, string databasename)
        {

            var cmd = db.CreateCommand();
            cmd.CommandText = DatabaseExistsSQL(GetFullDBName(databasename));
            var res = cmd.ExecuteScalar();
            long cnt = Convert.ToInt64(res);

            if (cnt <= 0)
            {
                var create_cmd = db.CreateCommand();
                create_cmd.CommandText = $"CREATE DATABASE {GetFullDBName(databasename)};";
                create_cmd.ExecuteNonQuery();
            }
        }

        private string DatabaseExistsSQL(string databasename)
        {
            switch (this.ServerType)
            {
                case SQLType.MSSQL:
                    return $"select count(*) from master.dbo.sysdatabases where name = '{databasename}'";
                case SQLType.POSTGRESQL:
                    return $"SELECT count(*) FROM pg_database WHERE datname = '{databasename}'";
                case SQLType.MYSQL:
                    return $"SELECT count(*) FROM information_schema.schemata WHERE schema_name = '{databasename}'";
                default:
                    return "";
            }
        }

        public enum SQLType
        {
            MSSQL, POSTGRESQL, MYSQL
        }
    }

}
