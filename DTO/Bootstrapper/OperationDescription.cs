﻿using Newtonsoft.Json;
using System;

namespace ITUtil.Common.Utils.Bootstrapper
{
    public class OperationDescription
    {
        //public enum fanoutType
        //{
        //    /// <summary>
        //    /// Do not fanout messages (for operations that are to be scaled across multiple instances) such as "do" operations and cRud operations.
        //    /// </summary>
        //    none,
        //    /// <summary>
        //    /// Do fanout, but only once per machine. i.e. multiple instances of this service on the same machine will compete to get the message (for CrUD operations where instances notify each other of changes within the same machine) 
        //    /// </summary>
        //    perMachine,
        //    /// <summary>
        //    /// Do fanout to every instance, even on the same machine (for CrUD operations, where the service have implemented code to handle multiple insert/deletes/updates on the same data)
        //    /// </summary>
        //    all
        //}
        ///// <summary>
        ///// Gets or sets the fanout type
        ///// </summary>
        //public fanoutType Faneout { get; set; }

        public OperationDescription() { }
        public OperationDescription(string operation, /*fanoutType fanout, */string description, Type dataInput, Type dataOutput, ITUtil.Common.DTO.MetaDataDefinitions requiredClaims, MicroserviceDelegates.ExecuteOperation execute)
        {
            this.operation = operation;
            this.description = description;
            this.dataInputType = dataInput;
            this.dataOutputType = dataOutput;
            //this.Faneout = fanout;
            this.Execute += execute;
            this.RequiredClaims = requiredClaims;
            if (this.RequiredClaims == null)
            {
                this.RequiredClaims = new DTO.MetaDataDefinitions(new DTO.MetaDataDefinition[] { });
            }
        }
        public string operation { get; set; } //ex. "Insert", "Update", "Delete", "Get" etc.
        public string description { get; set; } //developer description
        public Type dataInputType { get; set; } //json description
        public Type dataOutputType { get; set; } //json description
                                                      //public List<EventDescription> events { get; set; }

        public ITUtil.Common.DTO.MetaDataDefinitions RequiredClaims { get; set; }


        [JsonIgnore]
        public MicroserviceDelegates.ExecuteOperation Execute { get; }
    }

}
