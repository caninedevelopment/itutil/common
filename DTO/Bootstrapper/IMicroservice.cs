﻿using System.Collections.Generic;
using static ITUtil.Common.Utils.Bootstrapper.MicroserviceDelegates;

namespace ITUtil.Common.Utils.Bootstrapper
{
    public interface IMicroservice
    {
        /// <summary>
        /// Should contain a unique service name
        /// </summary>
        string ServiceName { get; }

        /// <summary>
        /// List of operations that can be performed by this service
        /// </summary>
        List<OperationDescription> OperationDescription { get; }

        /// <summary>
        /// Returns the version number for this implementation
        /// </summary>
        ITUtil.Common.DTO.ProgramVersion ProgramVersion { get; }

//        void SetSettings(GlobalRessources settings);
    }

}
