﻿using System;
using System.Collections.Generic;
using System.Text;
using ITUtil.Common.DTO;

namespace ITUtil.Common.Utils.Bootstrapper
{
    public class MicroserviceDelegates
    {
        public delegate void RaiseEvent(string eventname, object eventDTO);
        public delegate void LogEvent(string eventname, string message, ProgramVersion version, ITUtil.Common.DTO.Severity severity, Guid organisation);
        public delegate bool CheckCredentials(MessageWrapper messageWrapper, ITUtil.Common.DTO.MetaDataDefinition claim);
        public delegate ReturnMessageWrapper ExecuteOperation(MessageWrapper wrapper);
        public delegate void EventHandler(string[] topicparts, ReturnMessageWrapper/*EventDTO*/ data);
    }

}
