﻿// <copyright file="MessageWrapper.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    /// <summary>
    /// Message wrapper helper, for encoding/decoding data for transport
    /// </summary>
    /// <typeparam name="T">Type of the data</typeparam>
    public class MessageWrapperHelper<T>
    {
        /// <summary>
        /// Get decoded data from the message wrapper
        /// </summary>
        /// <param name="wrapper">message wrapper</param>
        /// <returns>decoded data</returns>
        public static T GetData(MessageWrapper wrapper)
        {
            T data = default(T);
            string json = System.Text.Encoding.UTF8.GetString(wrapper.messageData);
            data = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);

            return data;
        }

        /// <summary>
        /// Sets, and encode data into the message wrapper
        /// </summary>
        /// <param name="wrapper">message wrapper</param>
        /// <param name="data">encoding data</param>
        public static void SetData(MessageWrapper wrapper, T data)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            byte[] res = System.Text.Encoding.UTF8.GetBytes(json);
            wrapper.messageData = res;
        }
    }
}