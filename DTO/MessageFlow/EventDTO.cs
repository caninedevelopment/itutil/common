﻿// <copyright file="EventDTO{T}.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

using System;

namespace ITUtil.Common.DTO
{
    //    /// <summary>
    //    /// Event DTO definition
    //    /// </summary>
    //    public class EventDTO
    //    {
    //        /// <summary>
    //        /// Initializes a new instance of the <see cref="EventDTO"/> class.
    //        /// </summary>
    //        public EventDTO()
    //        {
    //        }

    //        /// <summary>
    //        /// Initializes a new instance of the <see cref="EventDTO"/> class.
    //        /// </summary>
    //        /// <param name="json">event data as json</param>
    //        /// <param name="clientid">caller clientid</param>
    //        /// <param name="messageid">caller messageid</param>
    //        public EventDTO(string json, string clientid, string messageid)
    //        {
    //            this.data = System.Text.Encoding.UTF8.GetBytes(json);
    //            this.clientId = clientid;
    //            this.messageId = messageid;
    //        }



    //        /// <summary>
    //        /// Initializes a new instance of the <see cref="EventDTO"/> class.
    //        /// </summary>
    //        /// <param name="obj">event data</param>
    //        /// <param name="clientid">caller clientid</param>
    //        /// <param name="messageid">caller messageid</param>
    //        public EventDTO(ReturnMessageWrapper obj, string clientid, string messageid)
    //        {
    //            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
    //            this.data = System.Text.Encoding.UTF8.GetBytes(json);
    //            this.clientId = clientid;
    //            this.messageId = messageid;
    //        }

    //        /// <summary>
    //        /// Initializes a new instance of the <see cref="EventDTO"/> class.
    //        /// </summary>
    //        /// <param name="obj">event data</param>
    //        /// <param name="messageWrapper">messageWrapper</param>
    //        public EventDTO(ReturnMessageWrapper obj, MessageWrapper messageWrapper)
    //        {
    //            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
    //            this.data = System.Text.Encoding.UTF8.GetBytes(json);
    //            this.clientId = messageWrapper.clientId;
    //            this.messageId = messageWrapper.messageId;
    //        }

    //        public ReturnMessageWrapper GetData()
    //        {
    //            return Utils.MessageDataHelper.FromMessageData<ReturnMessageWrapper>(this.data);
    //        }

    //        /// <summary>
    //        /// Gets or sets ClientId
    //        /// </summary>
    //        public string clientId { get; set; }

    //        /// <summary>
    //        /// Gets or sets MessageId
    //        /// </summary>
    //        public string messageId { get; set; }

    //        /// <summary>
    //        /// Gets or sets Data
    //        /// </summary>
    //        public byte[] data { get; set; }


    //        /// <summary>
    //        /// Get event data as json
    //        /// </summary>
    //        /// <returns>json string</returns>
    //        public string Json()
    //        {
    //            return System.Text.Encoding.UTF8.GetString(this.data);
    //        }
    //    }

    public class EventCallbackDto
    {
        public Guid id { get; set; }
        public string route { get; set; }
    }
}
