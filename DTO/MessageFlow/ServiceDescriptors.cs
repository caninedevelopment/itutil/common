﻿namespace ITUtil.Common.DTO.MessageFlow
{
    using System.Collections.Generic;
    using ITUtil.Common.Utils.Bootstrapper;

    public class ServiceDescriptors
    {
        public List<ServiceDescriptor> Services { get; set; } = new List<ServiceDescriptor>();
    }

    public class ServiceDescriptor
    {
        public ServiceDescriptor()
        {
            this.operationDescriptions = new List<OperationDescription>();
        }

        public ServiceDescriptor(ProgramVersion version, OperationDescription operationDescription)
        {
            this.version = version;
            this.operationDescriptions = new List<OperationDescription>() { operationDescription };
        }

        public ServiceDescriptor(OperationDescription operationDescription)
        {
            this.operationDescriptions = new List<OperationDescription>() { operationDescription };
        }
        public string serviceName { get; set; }
        public string routeNamespaces { get; set; }


        public ProgramVersion version { get; set; }

        public List<OperationDescription> operationDescriptions { get; set; }
    }
}
