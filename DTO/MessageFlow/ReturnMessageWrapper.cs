﻿// <copyright file="ReturnMessageWrapper.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    /// <summary>
    /// Wrapper for return messages
    /// </summary>
    public class ReturnMessageWrapper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReturnMessageWrapper"/> class.
        /// </summary>
        /// <param name="responseToClientid">Which client is to receive the message</param>
        /// <param name="responseToMessageid">What is the clients message id</param>
        /// <param name="data">the return object as json (utf8 byte array)</param>
        public ReturnMessageWrapper(string responseToClientid, string responseToMessageid, byte[] data)
        {
            this.responseToClientId = responseToClientid;
            this.responseToMessageId = responseToMessageid;
            this.data = data;
        }

        public static ReturnMessageWrapper CreateResult(bool success, ITUtil.Common.DTO.MessageWrapper wrapper, object data, params LocalizedString[] message)
        {
            string dataAsJson = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            byte[] res = System.Text.Encoding.UTF8.GetBytes(dataAsJson);

            ITUtil.Common.DTO.ReturnMessageWrapper resultobj = new ReturnMessageWrapper(wrapper.clientId, wrapper.messageId, res);
            resultobj.message = message;
            resultobj.success = success;
            return resultobj;
        }


        /// <summary>
        /// Gets or sets ResponseToClientid
        /// </summary>
        public string responseToClientId { get; set; }

        /// <summary>
        /// Gets or sets ResponseToMessageid
        /// </summary>
        public string responseToMessageId { get; set; }

        /// <summary>
        /// Gets or sets Message
        /// </summary>
        public LocalizedString[] message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets Success
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// Gets or sets data (json serialized object, which have been converted to utf8 byte array)
        /// </summary>
        public byte[] data { get; set; }
    }
}