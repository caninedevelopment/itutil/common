﻿// <copyright file="Event.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
namespace ITUtil.Common.DTO
{
    using System;

    /// <summary>
    /// Event definition
    /// </summary>
    public class Event
    {
        ///// <summary>
        ///// Gets or sets servicename
        ///// </summary>
        //public string serviceName { get; set; }

        ///// <summary>
        ///// Gets or sets servicename
        ///// </summary>
        //public string serviceVersion { get; set; }

        ///// <summary>
        ///// Gets or sets Servername
        ///// </summary>
        //public string serverName { get; set; }

        /// <summary>
        /// Gets or sets Timestamp
        /// </summary>
        public DateTime timeStamp { get; set; }

        /// <summary>
        /// Gets or sets Eventtitel
        /// </summary>
        public string eventTitel { get; set; }

        /// <summary>
        /// Gets or sets Eventmessage
        /// </summary>
        public string eventMessage { get; set; }

        ///// <summary>
        ///// Gets or sets Severity
        ///// </summary>
        //public Severity severity { get; set; }

        ///// <summary>
        ///// Gets or sets Metadata
        ///// </summary>
        //public MetaData[] metadata { get; set; }
    }
}