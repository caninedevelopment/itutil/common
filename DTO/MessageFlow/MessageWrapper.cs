﻿// <copyright file="MessageWrapper.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    using System;

    /// <summary>
    /// Message wrapper definition
    /// </summary>
    public class MessageWrapper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWrapper"/> class.
        /// </summary>
        public MessageWrapper()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWrapper"/> class.
        /// by cloning existing messageWrapper
        /// </summary>
        /// <param name="messageWrapper">
        /// The messageWrapper to be cloned
        /// </param>
        public MessageWrapper(MessageWrapper messageWrapper)
        {
            this.headerInfo = messageWrapper.headerInfo;
            this.messageData = messageWrapper.messageData;
            this.tracing = messageWrapper.tracing;
        //    this.organisationContext = messageWrapper.organisationContext;
            this.clientId = messageWrapper.clientId;
            this.messageId = messageWrapper.messageId;
            this.callerIp = messageWrapper.callerIp;
            this.issuedDate = messageWrapper.issuedDate;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWrapper"/> class.
        /// </summary>
        /// <param name="headerinfo">auth information for this message</param>
        /// <param name="issuedDate">message issuedate</param>
        /// <param name="userContext">user that issued the message</param>
        /// <param name="clientid">client id, for returning messages</param>
        /// <param name="messageid">message id, defined by client</param>
        /// <param name="callerIp">IP of the caller</param>
        /// <param name="orgContext">Organisation context</param>
        /// <param name="tracing">tracing level</param>
        public MessageWrapper(TokenData headerinfo, DateTime issuedDate, Guid userContext, string clientid, string messageid, string callerIp, Tracing tracing = null)
        {
            this.Init(headerinfo, clientid, messageid, callerIp, null, issuedDate, tracing);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWrapper"/> class.
        /// </summary>
        /// <param name="headerinfo">auth information for this message</param>
        /// <param name="issuedDate">message issuedate</param>
        /// <param name="clientid">client id, for returning messages</param>
        /// <param name="messageid">message id, defined by client</param>
        /// <param name="callerIp">IP of the caller</param>
        /// <param name="json">json</param>
        /// <param name="tracing">tracing level</param>
        public MessageWrapper(TokenData headerinfo, DateTime issuedDate, string clientid, string messageid, string callerIp, string json, Tracing tracing = null)
        {
            this.Init(headerinfo, clientid, messageid, callerIp, System.Text.Encoding.UTF8.GetBytes(json), issuedDate,  tracing);
        }

        public TokenData headerInfo { get; set; }

        public bool isDirectLink { get; set; }

        /// <summary>
        /// Gets or sets MessageData
        /// </summary>
        public byte[] messageData { get; set; }

        /// <summary>
        /// Gets or sets the program version for the request (which program version to be activated on the server)
        /// </summary>
        public ProgramVersion version { get; set; }

        /// <summary>
        /// Gets or sets Tracing
        /// </summary>
        public Tracing tracing { get; set; }

        /// <summary>
        /// Gets or sets OrgContext
        /// </summary>
       // public Guid organisationContext { get; set; }

        /// <summary>
        /// Gets or sets Clientid
        /// </summary>
        public string clientId { get; set; }

        /// <summary>
        /// Gets or sets Messageid
        /// </summary>
        public string messageId { get; set; }

        /// <summary>
        /// Gets or sets CallerIp
        /// </summary>
        public string callerIp { get; set; }

        /// <summary>
        /// Gets or sets IssuedDate
        /// </summary>
        public DateTime issuedDate { get; set; }

        private void Init(TokenData headerinfo, string clientid, string messageid, string callerIp, byte[] data, DateTime issuedDate,  Tracing tracing)
        {
            this.isDirectLink = false;
            this.headerInfo = headerinfo;
            this.issuedDate = issuedDate;
            this.messageData = data;
            this.tracing = tracing;
            this.clientId = clientid;
            this.messageId = messageid;
            this.callerIp = callerIp;
         //   this.organisationContext = orgContext;
        }
    }
}