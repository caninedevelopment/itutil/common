﻿// <copyright file="Tracing.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    /// <summary>
    /// Tracing description
    /// </summary>
    public class Tracing
    {
        /// <summary>
        /// Tracing level
        /// </summary>
        public enum Level
        {
            /// <summary>
            /// Dont trace anything
            /// </summary>
            none,

            /// <summary>
            /// Trace only the network/route information
            /// </summary>
            network,

            /// <summary>
            /// Trace everything
            /// </summary>
            all
        }

        /// <summary>
        /// Gets or sets trace level
        /// </summary>
        public Level traceLevel { get; set; }

        /// <summary>
        /// Gets or sets trace
        /// </summary>
        public Trace[] trace { get; set; }
    }
}