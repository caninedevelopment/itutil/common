﻿// <copyright file="Trace.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    /// <summary>
    /// Object used to transfer trace information
    /// </summary>
    public class Trace
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Trace"/> class.
        /// Default constructor used by the deserialiser
        /// </summary>
        public Trace()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Trace"/> class.
        /// </summary>
        /// <param name="IP">the IP adress of the machine running the program</param>
        /// <param name="service">Name of the microservice that created this trace</param>
        /// <param name="topic">A brief description of the trace, would normally be the message-route</param>
        public Trace(string ip, string service, string topic, string messageId)
        {
            this.ip = ip;
            this.service = service;
            this.topic = topic;
            this.messageId = messageId;
        }

        /// <summary>
        /// Gets or sets the Ip adress
        /// </summary>
        public string ip { get; set; }

        /// <summary>
        /// Gets or sets the messageId
        /// </summary>
        public string messageId { get; set; }

        /// <summary>
        /// Gets or sets name of the service
        /// </summary>
        public string service { get; set; }

        /// <summary>
        /// Gets or sets the topic
        /// </summary>
        public string topic { get; set; }

        /// <summary>
        /// Gets or sets the traceinformation
        /// </summary>
        public string internalTrace { get; set; }
    }
}