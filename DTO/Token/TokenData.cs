﻿// <copyright file="TokenData.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.DTO
{
    using System;

    /// <summary>
    /// TokenData description
    /// </summary>
    public class TokenData
    {
        /// <summary>
        /// Gets or sets Tokenid
        /// </summary>
        public Guid tokenId { get; set; }

        /// <summary>
        /// Gets or sets Userid
        /// </summary>
        public Guid userId { get; set; }

        /// <summary>
        /// Gets or sets ValidUntil
        /// </summary>
        public DateTime validUntil { get; set; }

        /// <summary>
        /// Gets or sets Claims
        /// </summary>
        public ITUtil.Common.DTO.MetaData[] claims { get; set; }

        /// <summary>
        /// Gets or sets OrganisationClaims
        /// </summary>
        //public ITUtil.Common.DTO.MetaData[] OrganisationClaims { get; set; }

        public bool IsValidToken()
        {
            return this.validUntil > DateTime.Now;
        }

    }
    }
