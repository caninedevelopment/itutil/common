﻿// <copyright file="ConfigHelper.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.Utils
{
    using System.IO;

    /// <summary>
    /// A helper class for easy access to the config file
    /// </summary>
    public class ConfigHelper
    {
        //private static string config = null;
        /// <summary>
        /// Access to the configuration file
        /// </summary>
        /// <returns>The application configuration object</returns>
        public static string Config(string settingsFilename)
        {
            //if (config == null)
            //{
            string filename = Directory.GetCurrentDirectory() + "/" + (string.IsNullOrEmpty(settingsFilename) ? "appsettings.json" : settingsFilename);
            if (System.IO.File.Exists(filename))
            {
                return System.IO.File.ReadAllText(filename);
            }
            else
            {
                return null;
            }
            //}

            //return config;
        }
    }
}