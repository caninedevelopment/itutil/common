﻿// <copyright file="ExceptionHelper.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.Utils
{
    using System;
    using System.Text;

    /// <summary>
    /// Exception helper, to log stacktrace and exception-stack
    /// </summary>
    public class ExceptionHelper
    {
        /// <summary>
        /// Gets exception as string
        /// </summary>
        /// <param name="ex">Exception to tranform into text</param>
        /// <returns>exception string</returns>
        public static string GetExceptionAsReportText(Exception ex)
        {
            StringBuilder exception = new StringBuilder();
            StringBuilder trace = new StringBuilder();
            Exception e = ex;
            while (e != null)
            {
                exception.AppendLine(e.Message);
                trace.AppendLine(e.StackTrace);
                e = e.InnerException;
            }

            string reporttxt = string.Format("Exception: {0} \r\n Trace: {1}", exception, trace);
            return reporttxt;
        }
    }
}