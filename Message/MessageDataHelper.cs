﻿// <copyright file="MessageDataHelper.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.Utils
{
    /// <summary>
    /// Class containing methods for helping deserializing and serializing objects and bytearrays.
    /// </summary>
    public class MessageDataHelper
    {
        /// <summary>
        /// Takes the object and returns the bytearray of the json
        /// </summary>
        /// <param name="obj">The object parameter to be converted</param>
        /// <returns>Json as utf8 bytearray</returns>
        public static byte[] ToMessageData(object obj)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            return System.Text.Encoding.UTF8.GetBytes(json);
        }

        /// <summary>
        /// Takes the bytearray and returns the object as a typed object
        /// </summary>
        /// <typeparam name="T">The Type of the returned object</typeparam>
        /// <param name="bytes">The bytearray that will be returned as a typed object</param>
        /// <returns>Object of specified generic type from the bytearray parameter</returns>
        public static T FromMessageData<T>(byte[] bytes)
        {
            var jsonStr = System.Text.Encoding.UTF8.GetString(bytes);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonStr);
        }
    }
}
