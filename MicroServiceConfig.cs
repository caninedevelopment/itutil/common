﻿// <copyright file="ConfigHelper.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Common.Utils
{
    using ITUtil.Common.Utils.Bootstrapper;
    using System;

    public static class MicroServiceConfig
    {
//        private static GlobalRessources config = null;
        public static GlobalRessources getConfig(string settingsFilename)
        {
            // if (config == null)
            // {
            string json = ConfigHelper.Config(settingsFilename);
            if (json != null)
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<GlobalRessources>(json);
            }
            else

            {
                return null;
            }
            // }
            // return config;

        }

        //public static string Rabbit_hostname()
        //{
        //    return getConfig().rabbitMqHostname;
        //}

        //public static string Rabbit_username()
        //{
        //    return getConfig().rabbitMqUsername;
        //}

        //public static string Rabbit_password()
        //{
        //    return getConfig().rabbitMqPassword;
        //}

        //public static Guid ServiceUID()
        //{
        //    return getConfig().serviceGuid;
        //}
    }

    //public class StandardConfiguration
    //{
    //    public string rabbitMqHostname { get; set; }
    //    public string rabbitMqUsername { get; set; }
    //    public string rabbitMqPassword { get; set; }
    //    public Guid serviceGuid { get; set; }
    //}
}